package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func dayone() {
	f, _ := os.Open("input.txt")

	defer f.Close()

	scanner := bufio.NewScanner(f)

	totalHighestCaloriesElf := 0
	secondBestCaloriesElf := 0
	thirdBestCaloriesElf := 0
	currentCaloriesCounter := 0

	for scanner.Scan() {
		if scanner.Text() == "" {

			if totalHighestCaloriesElf < currentCaloriesCounter {
				thirdBestCaloriesElf = secondBestCaloriesElf
				secondBestCaloriesElf = totalHighestCaloriesElf
				totalHighestCaloriesElf = currentCaloriesCounter

			} else if secondBestCaloriesElf < currentCaloriesCounter {
				thirdBestCaloriesElf = secondBestCaloriesElf
				secondBestCaloriesElf = currentCaloriesCounter
			} else if thirdBestCaloriesElf < currentCaloriesCounter {
				thirdBestCaloriesElf = currentCaloriesCounter
			}

			currentCaloriesCounter = 0

		} else {
			elfCalories, _ := strconv.ParseInt(scanner.Text(), 0, 24)
			currentCaloriesCounter += int(elfCalories)
		}
	}

	fmt.Println(totalHighestCaloriesElf + secondBestCaloriesElf + thirdBestCaloriesElf)
}
