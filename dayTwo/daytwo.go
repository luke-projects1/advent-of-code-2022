package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	f, _ := os.Open("input.txt")

	defer f.Close()

	scanner := bufio.NewScanner(f)

	totalpoints := 0

	for scanner.Scan() {
		inputRowSplitted := strings.Split(scanner.Text(), " ")
		opponent := inputRowSplitted[0]
		ourChoice := inputRowSplitted[1]

		totalpoints += WhoWon(strings.ToUpper(opponent), strings.ToUpper(ourChoice))

	}

	fmt.Println(totalpoints)
}

func WhoWon(opponent string, ourPick string) int {

	totalToReturn := 0
	switch {
	// Rock & Rock
	case opponent == "A" && ourPick == "X": // Scissors
		//totalToReturn = (3 + 1)
		totalToReturn = (0 + 3)
	// Rock & Paper
	case opponent == "A" && ourPick == "Y": // Rock
		//totalToReturn = (6 + 2)
		totalToReturn = (3 + 1)
	// Rock & Scissors
	case opponent == "A" && ourPick == "Z": // Paper
		//totalToReturn = (0 + 3)
		totalToReturn = (6 + 2)

	// Paper & Rock
	case opponent == "B" && ourPick == "X":
		totalToReturn = (0 + 1)
	// Paper & Paper
	case opponent == "B" && ourPick == "Y":
		totalToReturn = (3 + 2)
	// Paper & Scissors
	case opponent == "B" && ourPick == "Z":
		totalToReturn = (6 + 3)

	// Scissors & Rock
	case opponent == "C" && ourPick == "X": // paper
		//totalToReturn = (6 + 1)
		totalToReturn = (0 + 2)
	// Scissors & Paper
	case opponent == "C" && ourPick == "Y": // Scissors
		//totalToReturn = (0 + 2)
		totalToReturn = (3 + 3)
	// Scissors & Scissors
	case opponent == "C" && ourPick == "Z": // Rock
		//totalToReturn = (3 + 3)
		totalToReturn = (6 + 1)
	}

	return totalToReturn
}
